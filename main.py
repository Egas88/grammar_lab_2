matrix = [  # S T B + * ( a ) #
    [0, 0, 0, 2, 2, 0, 0, 2, 0],
    [0, 0, 0, 3, 3, 0, 0, 3, 3],
    [0, 0, 0, 3, 3, 0, 0, 3, 3],
    [0, 2, 0, 0, 0, 1, 1, 0, 0],
    [0, 2, 0, 0, 0, 1, 1, 0, 0],
    [1, 1, 2, 0, 0, 1, 1, 0, 0],
    [0, 0, 0, 3, 3, 0, 0, 3, 3],
    [0, 0, 0, 3, 3, 0, 0, 3, 3],
    [1, 1, 0, 0, 0, 1, 1, 0, 0]]

grammar = {
    'S+T': 'S',
    'S*T': 'S',
    'T': 'S',
    '(B': 'T',
    'a': 'T',
    'S)': 'B'
}

symbol = {
    "S": 0,
    "T": 1,
    "B": 2,
    "+": 3,
    "*": 4,
    "(": 5,
    "a": 6,
    ")": 7,
    "#": 8
}


def get_relation(left, right):
    res = matrix[symbol[left]][symbol[right]]
    return res


def main():
    print("Enter a line")
    str1 = input() + "#"
    stack = ['#']
    pointer = 0
    count = 0
    while pointer < len(str1):
        char = str1[pointer]
        print(f'Шаг: {count}, Стек: {stack}, Цепочка: {str1}')
        if stack == ['#', 'S'] and char == '#':  # Line convolved to #A# i.e.  successful recognition
            return True
        if char not in symbol:
            print('ERROR: Such symbol does not exist in this grammar...')
            return False
        relation = get_relation(stack[-1], char)
        if relation == 1 or relation == 2:
            stack.append(char)
            pointer += 1
        elif relation == 3:
            temp_stack = []
            stack_getted = stack.pop()
            temp_stack.append(stack_getted)
            while matrix[symbol[stack[-1]]][symbol[stack_getted]] != 1:
                stack_getted = stack.pop()
                temp_stack.append(stack_getted)
            temp_stack.reverse()
            rule = ''.join(temp_stack)
            try:
                rem = grammar[rule]
            except:
                print('ERROR: Such rule does not exist in this grammar...')
                return False
            stack.append(rem)
        else:
            print('ERROR: there is no such relation...')
            return False
        count += 1


if __name__ == "__main__":
    result = main()
    if not result:
        print("Line is NOT a part of the grammar")
    else:
        print("Line IS a part of the grammar")
